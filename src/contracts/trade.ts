import {
    assert,
    ByteString,
    method,
    prop,
    PubKey,
    hash160,
    Sig,
    SmartContract,
    PubKeyHash,
    exit,
    byteString2Int,
    toByteString,
    sha256,
    Utils,
    hash256,
    FixedArray
} from 'scrypt-ts'

export class TOKENOVATE_KSAA_SPOT_ASM extends SmartContract {
    // Tokenovate KSAA-SPOT script
    // tradeJSON: 7b0a20202273657373696f6e4964223a202264383833343163372d643439662d343333362d623939312d633131313966336231323131222c0a2020226275796572223a207b0a20202020227075626c69634b6579223a2022303230323032303230323032303230323032303230323032303230323032303230323032303230323032303230323032303230323032303230323032303230323032222c0a2020202022637573746f6469616e223a2022456c61732062616e6b220a20207d2c0a20202273656c6c6572223a207b0a20202020227075626c69634b6579223a2022303230333033303330333033303330333033303330333033303330333033303330333033303330333033303330333033303330333033303330333033303330333033222c0a2020202022637573746f6469616e223a2022456c617320636172626f6e220a20207d2c0a202022657865637574696f6e4964223a20223233343538353230222c0a20202273796d626f6c223a20224b5341412d53504f54222c0a2020227175616e74697479223a202231332e3030303030222c0a2020227072696365223a2022313733332e333231222c0a2020227472616e73616374696f6e54696d65223a2022323032332d30342d30315432333a33303a30342e3036385a222c0a2020226d73674f726967696e223a2022474d4558222c0a2020226d7367554944223a2022646a336e30392d386562632d343866632d396438632d346461376239626561636239222c0a2020226d736754797065223a20225452414445222c0a2020226d736754696d657374616d70223a20313638333036363630343737352c0a20202263617074757265644174223a20313638333036363630353832322c0a2020226576656e744964223a202239303865653263352d613034312d346665632d623436362d356632303661363936626363220a7d
    // breakdownOffsets: 1224224215092742150b1908100912080f081918130410221105150d120d10240300000000
    @prop() pubKey: PubKey
    @prop() adminPubKeyHash: PubKeyHash
    @prop() static messageSegmentHash: ByteString = toByteString('d5c10a6bcb6f1122b49474822d75b6e27a704fd8525a96aec73b0a9c17adfb2a')
    @prop() static symbol: ByteString = toByteString('KSAA-SPOT', true)
    @prop() static msgOrigin: ByteString = toByteString('GMEX', true)
    @prop() static msgType: ByteString = toByteString('TRADE', true)
    // @prop() static lenSplit1: ByteString = toByteString('68')
    // @prop() static scriptSig: ByteString = toByteString('ac')
    @prop() static receiptPt1: ByteString = toByteString('Tokenovate have created a new ', true)
    @prop() static receiptPt2: ByteString = toByteString(' event between: Buyer: ', true)
    @prop() static receiptPt3: ByteString = toByteString(' buyer custodian: ', true)
    @prop() static receiptPt4: ByteString = toByteString(' and Seller:', true)
    @prop() static receiptPt5: ByteString = toByteString(' Seller custodian: ', true)
    @prop() static receiptPt6: ByteString = toByteString(' For ', true)
    @prop() static receiptPt7: ByteString = toByteString(' units ', true)
    @prop() static receiptPt8: ByteString = toByteString(' At a price of ', true)
    @prop() static receiptPt9: ByteString = toByteString(' USD each. The trade venue is ', true)
    @prop() static receiptPt10: ByteString = toByteString(' The trade was completed at ', true)
    @prop() static receiptPt11: ByteString = toByteString(' For more information see https://receiptviewer.tokenovate.com/trade/', true)
    @prop() static receiptPt12: ByteString = toByteString(' KSAA-SPOT trade version 0.1 this trade is recorded on the Tokenovate Trade Ledger', true)
    @prop() static falseReturn: ByteString = toByteString('006a4d')
    @prop() static scriptValueSize: ByteString = toByteString('0000000000000000fd')

    constructor(pubKey: PubKey, adminPubKeyHash: PubKeyHash) {
        super(...arguments)
        this.pubKey = pubKey
        this.adminPubKeyHash = adminPubKeyHash
    }

    @method()
    public unlock(
        sig: Sig,
        pubKey: PubKey,
        // context: SigHashPreimage,
        tradeJSON: ByteString,
        breakdownOffsets: ByteString
    ) {
        // - Drops tokenovate off stack (SKIPPING)
        // - Checks if admin pukey if so checksig that and return
        if (hash160(pubKey) == this.adminPubKeyHash) {
            if (this.checkSig(sig, pubKey)) exit(true)
        }
        // - Otherwise checksig against pubKey
        // Check public keys belong to the specified addresses
        assert(this.checkSig(sig, pubKey), 'Spender sig invalid')

        // - extract data from specifically ordered json
        let offsetIdx = 0n
        let jsonIdx = 0n
        // Now we loop through the total amount of (flattened) json keys (16)
        let offsetByte: ByteString = toByteString('')
        let offsetIntValue = 0n
        let lenByte: ByteString = toByteString('')
        let len: bigint = 0n
        let messageSegments: ByteString = toByteString('')


        // Now we skip through the json extracting the array of values (after we manually extract the first)
        offsetByte = breakdownOffsets.slice(Number(offsetIdx) * 2, Number(offsetIdx + 1n) * 2)
        offsetIntValue = byteString2Int(offsetByte)
        jsonIdx += offsetIntValue
        offsetIdx += 1n
        lenByte = breakdownOffsets.slice(Number(offsetIdx) * 2, Number(offsetIdx + 1n) * 2)
        len = byteString2Int(lenByte)
        offsetIdx += 1n
        messageSegments = tradeJSON.slice(
            0,
            Number(jsonIdx) * 2
        )
        // const sessionId: ByteString = tradeJSON.slice(
        //     Number(jsonIdx) * 2,
        //     Number(jsonIdx + len) * 2
        // )
        jsonIdx += len
        let jsonDataPushes: FixedArray<ByteString, 15> = [toByteString(''),toByteString(''),toByteString(''),toByteString(''),toByteString(''),toByteString(''),toByteString(''),toByteString(''),toByteString(''),toByteString(''),toByteString(''),toByteString(''),toByteString(''),toByteString(''),toByteString('')]
        for (let i = 0n; i < 15; i++) {
            offsetByte = breakdownOffsets.slice(Number(offsetIdx) * 2, Number(offsetIdx + 1n) * 2)
            offsetIntValue = byteString2Int(offsetByte)
            messageSegments += tradeJSON.slice(Number(jsonIdx) * 2, Number(jsonIdx + offsetIntValue) * 2)
    
            jsonIdx += offsetIntValue
            offsetIdx += 1n
            lenByte = breakdownOffsets.slice(Number(offsetIdx) * 2, Number(offsetIdx + 1n) * 2)
            len = byteString2Int(lenByte)
            offsetIdx += 1n
    
            jsonDataPushes[Number(i)] = tradeJSON.slice(
                Number(jsonIdx) * 2,
                Number(jsonIdx + len) * 2
            )
            jsonIdx += len
        }

        const buyerPublicKey: ByteString = jsonDataPushes[0]
        const buyerCustodian: ByteString = jsonDataPushes[1]
        const sellerPublicKey: ByteString = jsonDataPushes[2]
        const sellerCustodian: ByteString = jsonDataPushes[3]
        const executionId: ByteString = jsonDataPushes[4]
        const symbol: ByteString = jsonDataPushes[5]
        const quantity: ByteString = jsonDataPushes[6]
        const price: ByteString = jsonDataPushes[7]
        const transactionTime: ByteString = jsonDataPushes[8]
        const msgOrigin: ByteString = jsonDataPushes[9]
        // const msgUID: ByteString = jsonDataPushes[10]
        const msgType: ByteString = jsonDataPushes[11]
        // const msgTimestamp: ByteString = jsonDataPushes[12]
        // const captureTimestamp: ByteString = jsonDataPushes[13]
        // const eventID: ByteString = jsonDataPushes[14]
        messageSegments += tradeJSON.slice(Number(jsonIdx) * 2, Number(jsonIdx + 3n) * 2)
        // The string of non-data items is then hashed and checked against an expected 
        // value. This ensures that only valid JSON packets in the expected format can 
        // activate the contract.

        // We don't care about session-id
        // We care only about buyer pubKey, buyer custodian, seller pubKey, seller custodian, and 
        // executionID
        let messageHash: ByteString = sha256(messageSegments)
        assert(messageHash == TOKENOVATE_KSAA_SPOT_ASM.messageSegmentHash, 'assert message hash failure')
        // // Next are buyer pubKey, buyer custodian, seller pubKey, seller custodian, and 
        // // executionID which we keep to enter into the receipt
        // // Next is the symbol which we check is equal to ‘KSAA-SPOT’
        assert(symbol == TOKENOVATE_KSAA_SPOT_ASM.symbol, 'assert symbol failure')
        // // Next are quantity, price and transaction time which we keep for the receipt
        // // Next is the msgOrigin which we check is equal to GMEX
        assert(msgOrigin == TOKENOVATE_KSAA_SPOT_ASM.msgOrigin, 'assert origin failure')
        // // We check that msgType is ‘TRADE’
        assert(msgType == TOKENOVATE_KSAA_SPOT_ASM.msgType, 'assert msgType failure')

        // // We now check that the value of the input was 1 satoshi
        assert(this.ctx.utxo.value===1n)
        // Now we begin to construct the receipt.
        // Each code segment adds a section of text and the relevant data value from the 
        // stack to the message.
        // // Tokenovate have created a new TRADE
        const receipt: ByteString = TOKENOVATE_KSAA_SPOT_ASM.receiptPt1 + msgType + TOKENOVATE_KSAA_SPOT_ASM.receiptPt2 +
            buyerPublicKey + TOKENOVATE_KSAA_SPOT_ASM.receiptPt3 + buyerCustodian + TOKENOVATE_KSAA_SPOT_ASM.receiptPt4 +
            sellerPublicKey + TOKENOVATE_KSAA_SPOT_ASM.receiptPt5 + sellerCustodian + TOKENOVATE_KSAA_SPOT_ASM.receiptPt6 +
            quantity + TOKENOVATE_KSAA_SPOT_ASM.receiptPt7 + symbol + TOKENOVATE_KSAA_SPOT_ASM.receiptPt8 + price + TOKENOVATE_KSAA_SPOT_ASM.receiptPt9 + msgOrigin +
            TOKENOVATE_KSAA_SPOT_ASM.receiptPt10 + transactionTime + TOKENOVATE_KSAA_SPOT_ASM.receiptPt11 + executionId + TOKENOVATE_KSAA_SPOT_ASM.receiptPt12;

        const outputScript: ByteString = TOKENOVATE_KSAA_SPOT_ASM.falseReturn + TOKENOVATE_KSAA_SPOT_ASM.scriptValueSize + receipt
        const out = Utils.buildOutput(outputScript, this.ctx.utxo.value)
        assert(hash256(out) == this.ctx.hashOutputs, 'hashOutputs mismatch')
        //  OP_CODESEPARATOR is used to limit the transaction script length to 1 byte.
        // We need to check hash outs
    }

}
