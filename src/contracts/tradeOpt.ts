import {
    assert,
    ByteString,
    method,
    prop,
    PubKey,
    hash160,
    Sig,
    SmartContract,
    PubKeyHash,
    exit,
    byteString2Int,
    toByteString,
    sha256,
    Utils,
    hash256,
    FixedArray,
    len,
    int2ByteString
} from 'scrypt-ts'

export class TOKENOVATE_KSAA_SPOT_ASM extends SmartContract {
    @prop() pubKey: PubKey
    @prop() adminPubKeyHash: PubKeyHash
    constructor(pubKey: PubKey, adminPubKeyHash: PubKeyHash) {
        super(...arguments)
        this.pubKey = pubKey
        this.adminPubKeyHash = adminPubKeyHash
    }

    @method()
    public unlock(
        tradeJSON: ByteString,
        sig: Sig,
        pubKey: PubKey,
        breakdownOffsets: ByteString
    ) {
        // - Checks if admin pukey if so checksig that and return
        if (hash160(pubKey) == this.adminPubKeyHash) {
            if (this.checkSig(sig, pubKey)) exit(true)
        }
        // - Otherwise checksig against pubKey
        // Check public keys belong to the specified addresses
        assert(this.checkSig(sig, pubKey), 'Spender sig invalid')

        // - extract data from specifically ordered json
        let offsetIdx = 0n
        // Now we loop through the total amount of (flattened) json keys (16)
        let offsetIntValue = 0n

        // Now we skip through the json extracting the array of values (after we manually extract the first)
        let jsonIdx = byteString2Int(breakdownOffsets.slice(Number(offsetIdx) * 2, Number(offsetIdx + 1n) * 2))
        offsetIdx += 1n
        let lenPushData = byteString2Int(breakdownOffsets.slice(Number(offsetIdx) * 2, Number(offsetIdx + 1n) * 2))
        offsetIdx += 1n
        let messageSegments: ByteString = tradeJSON.slice(
            0,
            Number(jsonIdx) * 2
        )
        // const sessionId: ByteString = tradeJSON.slice(
        //     Number(jsonIdx) * 2,
        //     Number(jsonIdx + len) * 2
        // )
        jsonIdx += lenPushData
        let jsonDataPushes: FixedArray<ByteString, 15> = [toByteString(''), toByteString(''), toByteString(''), toByteString(''), toByteString(''), toByteString(''), toByteString(''), toByteString(''), toByteString(''), toByteString(''), toByteString(''), toByteString(''), toByteString(''), toByteString(''), toByteString('')]
        for (let i = 0n; i < 15; i++) {
            offsetIntValue = byteString2Int(breakdownOffsets.slice(Number(offsetIdx) * 2, Number(offsetIdx + 1n) * 2))
            messageSegments += tradeJSON.slice(Number(jsonIdx) * 2, Number(jsonIdx + offsetIntValue) * 2)
            jsonIdx += offsetIntValue
            offsetIdx += 1n
            lenPushData = byteString2Int(breakdownOffsets.slice(Number(offsetIdx) * 2, Number(offsetIdx + 1n) * 2))
            offsetIdx += 1n

            jsonDataPushes[Number(i)] = tradeJSON.slice(
                Number(jsonIdx) * 2,
                Number(jsonIdx + lenPushData) * 2
            )
            jsonIdx += lenPushData
        }
        
        // The string of non-data items is then hashed and checked against an expected 
        // value. This ensures that only valid JSON packets in the expected format can 
        // activate the contract.

        assert(sha256(messageSegments + tradeJSON.slice(Number(jsonIdx) * 2, Number(jsonIdx + 3n) * 2)) == toByteString('d5c10a6bcb6f1122b49474822d75b6e27a704fd8525a96aec73b0a9c17adfb2a'), 'assert message hash failure')
        // Next are buyer pubKey, buyer custodian, seller pubKey, seller custodian, and 
        // executionID which we keep to enter into the receipt
        // Next is the symbol which we check is equal to ‘KSAA-SPOT’
        assert(jsonDataPushes[5] == toByteString('KSAA-SPOT', true), 'assert symbol failure')
        // Next are quantity, price and transaction time which we keep for the receipt
        // Next is the msgOrigin which we check is equal to GMEX
        assert(jsonDataPushes[9] == toByteString('GMEX', true), 'assert origin failure')
        // // We check that msgType is ‘TRADE’
        assert(jsonDataPushes[11] == toByteString('TRADE', true), 'assert msgType failure')

        // We now check that the value of the input was 1 satoshi
        assert(this.ctx.utxo.value === 1n)
        // Now we begin to construct the receipt.
        // Each code segment adds a section of text and the relevant data value from the 
        // stack to the message.
        const outputScript: ByteString = toByteString('006a4d546f6b656e6f76617465206861766520637265617465642061206e657720') + jsonDataPushes[11] +
            toByteString('206576656e74206265747765656e3a0a42757965723a20') + jsonDataPushes[0] +
            toByteString('0a427579657220637573746f6469616e3a20') + jsonDataPushes[1] +
            toByteString('0a616e640a53656c6c65723a20') + jsonDataPushes[2] +
            toByteString('0a53656c6c657220637573746f6469616e3a20') + jsonDataPushes[3] +
            toByteString('0a0a666f7220') + jsonDataPushes[6] +
            toByteString('20756e69747320') + jsonDataPushes[5] +
            toByteString('2061742061207072696365206f662024') + jsonDataPushes[7] +
            toByteString('2055534420656163682e0a0a5468652074726164652076656e756520697320') + jsonDataPushes[9] +
            toByteString('0a0a546869732074726164652077617320636f6d706c6574656420617420') + jsonDataPushes[8] +
            toByteString('0a0a466f72206d6f726520696e666f726d6174696f6e207365653a2068747470733a2f2f726563656970747669657765722e746f6b656e6f766174652e636f6d2f74726164652f') + jsonDataPushes[4] +
            toByteString('0a4b5341412d53706f742074726164652056657273696f6e3a20302e310a54686973207472616465206973207265636f72646564206f6e2074686520546f6b656e6f76617465207472616465206c65646765722e')
        assert(hash256(int2ByteString(this.ctx.utxo.value, 8n) + toByteString('fd') + Utils.toLEUnsigned(BigInt(len(outputScript)), 2n) + outputScript) == this.ctx.hashOutputs, 'hashOutputs mismatch')
    }

}
