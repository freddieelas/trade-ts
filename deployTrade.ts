import { TOKENOVATE_KSAA_SPOT_ASM } from './src/contracts/trade'
import {
    bsv,
    TestWallet,
    DefaultProvider,
    PubKey,
    PubKeyHash,
    findSig,
    toHex,
    MethodCallOptions,
    toByteString,
} from 'scrypt-ts'

import * as dotenv from 'dotenv'

// Load the .env file
dotenv.config()

// Read the private key from the .env file.
// The default private key inside the .env file is meant to be used for the Bitcoin testnet.
// See https://scrypt.io/docs/bitcoin-basics/bsv/#private-keys
const privateKey = bsv.PrivateKey.fromWIF(process.env.PRIVATE_KEY || '')

// Prepare signer.
// See https://scrypt.io/docs/how-to-deploy-and-call-a-contract/#prepare-a-signer-and-provider
const signer = new TestWallet(
    privateKey,
    new DefaultProvider({
        network: bsv.Networks.testnet,
    })
)

async function main() {
    await TOKENOVATE_KSAA_SPOT_ASM.compile()

    // TODO: Adjust the amount of satoshis locked in the smart contract:
    const amount = 1000
    const adminWIF = 'cTWpMoCA7mrhG4L11j3RYdTUq2fkLxC4t6ZkZQKGrNt9yB3vUrE4'
    const adminKey = bsv.PrivateKey.fromWIF(adminWIF)
    const adminPubKeyHash = bsv.crypto.Hash.sha256ripemd160(
        adminKey.publicKey.toBuffer()
    ).toString('hex')

    // public key of the `privateKey`
    const publicKey = privateKey.publicKey
    const instance = new TOKENOVATE_KSAA_SPOT_ASM(
        // TODO: Adjust constructor parameter values:
        PubKey(publicKey.toHex()),
        PubKeyHash(adminPubKeyHash)
    )

    // Connect to a signer.
    await instance.connect(signer)

    // Contract deployment.
    const deployTx = await instance.deploy(amount)
    console.log(
        'Contract deployed: https://test.whatsonchain.com/tx/' + deployTx?.id
    )
    // call
    const { tx: callTx } = await instance.methods.unlock(
        /*
        sig: Sig,
        pubKey: PubKey,
        context: SigHashPreimage,
        tradeJSON: ByteString,
        breakdownOffsets: ByteString*/
        (sigResps) => findSig(sigResps, publicKey),
        PubKey(toHex(publicKey)),
        // SigHashPreimage(toHex(instance.ctx)), // ?????
        toByteString(
            '7b0a20202273657373696f6e4964223a202264383833343163372d643439662d343333362d623939312d633131313966336231323131222c0a2020226275796572223a207b0a20202020227075626c69634b6579223a2022303230323032303230323032303230323032303230323032303230323032303230323032303230323032303230323032303230323032303230323032303230323032222c0a2020202022637573746f6469616e223a2022456c61732062616e6b220a20207d2c0a20202273656c6c6572223a207b0a20202020227075626c69634b6579223a2022303230333033303330333033303330333033303330333033303330333033303330333033303330333033303330333033303330333033303330333033303330333033222c0a2020202022637573746f6469616e223a2022456c617320636172626f6e220a20207d2c0a202022657865637574696f6e4964223a20223233343538353230222c0a20202273796d626f6c223a20224b5341412d53504f54222c0a2020227175616e74697479223a202231332e3030303030222c0a2020227072696365223a2022313733332e333231222c0a2020227472616e73616374696f6e54696d65223a2022323032332d30342d30315432333a33303a30342e3036385a222c0a2020226d73674f726967696e223a2022474d4558222c0a2020226d7367554944223a2022646a336e30392d386562632d343866632d396438632d346461376239626561636239222c0a2020226d736754797065223a20225452414445222c0a2020226d736754696d657374616d70223a20313638333036363630343737352c0a20202263617074757265644174223a20313638333036363630353832322c0a2020226576656e744964223a202239303865653263352d613034312d346665632d623436362d356632303661363936626363220a7d'
        ),
        toByteString(
            '1224224215092742150b1908100912080f081918130410221105150d120d10240300000000'
        ),
        {
            pubKeyOrAddrToSign: publicKey,
        } as MethodCallOptions<TOKENOVATE_KSAA_SPOT_ASM>
    )

    console.log(
        'Contract called: https://test.whatsonchain.com/tx/' + callTx?.id
    )
}

main()
