const path = require("path");
const { readFileSync, existsSync, mkdirSync } = require("fs");
const {
  bsv,
  compile,
  compileContract: compileContractImpl,
  toHex,
  Int,
  JSUtil,
  isEmpty,
} = require("scryptlib");
var url_1 = require("url");

function path2uri(path) {
  if (isNode()) {
    return url_1.pathToFileURL(path).toString();
  } else {
    return path;
  }
}

function isNode() {
  return typeof window === "undefined" && typeof process === "object";
}

var fs = require("fs");

const { getCIScryptc } = require("scryptlib/dist/utils");

var os_1 = require("os");
const { exit } = require("process");
const minimist = require("minimist");

const Signature = bsv.crypto.Signature;
const BN = bsv.crypto.BN;
const Interpreter = bsv.Script.Interpreter;
const Opcode = bsv.Opcode;

// number of bytes to denote some numeric value
const DataLen = 1;

const axios = require("axios");
const API_PREFIX = "https://api.whatsonchain.com/v1/bsv/test";

const inputIndex = 0;
const inputSatoshis = 100000;
const flags =
  Interpreter.SCRIPT_VERIFY_MINIMALDATA |
  Interpreter.SCRIPT_ENABLE_SIGHASH_FORKID |
  Interpreter.SCRIPT_ENABLE_MAGNETIC_OPCODES |
  Interpreter.SCRIPT_ENABLE_MONOLITH_OPCODES;
const minFee = 546;
const dummyTxId =
  "a477af6b2667c29670467e4e0728b685ee07b240235771862318e29ddbe58458";
const reversedDummyTxId =
  "5884e5db9de218238671572340b207ee85b628074e7e467096c267266baf77a4";
const sighashType2Hex = (s) => s.toString(16);

const { Hash } = require("bsv");

function newTx() {
  const utxo = {
    txId: "a477af6b2667c29670467e4e0728b685ee07b240235771862318e29ddbe58458",
    outputIndex: 1,
    script: "", // placeholder
    satoshis: inputSatoshis,
  };
  return new bsv.Transaction().from(utxo);
}

function toLittleIndian(hexstr, padding = 0) {
  let s = reverseEndian(hexstr);

  while (s.length < padding) {
    s = s + "0";
  }
  return s;
}

const addOpDrops = (count) => {
  var asmDrops = [];
  var hexDrops = "";
  for (var i = count; i > 0; ) {
    if (i > 1) {
      asmDrops.push({
        opcode: "OP_2DROP",
        stack: undefined,
        topVars: [],
        pos: undefined,
        debugInfo: undefined,
      });
      hexDrops += "6d";
      i -= 2;
    } else {
      asmDrops.push({
        opcode: "OP_DROP",
        stack: undefined,
        topVars: [],
        pos: undefined,
        debugInfo: undefined,
      });
      hexDrops += "75";
      i -= 1;
    }
  }
  return { asmDrops, hexDrops };
};

const cloneArgsToEndOfScript = (compiledContractResult) => {
  let hex = compiledContractResult.hex;
  let asm = compiledContractResult.asm;
  console.log({
    compiledContractResult,
    hex,
    asm,
  });
  // Count the constructor arguments
  let argCount = 0;
  // First we modify the contracts ASM
  let asmPostProcessingCompleted = false;
  while (!asmPostProcessingCompleted) {
    const cursor = asm[argCount];
    if (cursor.opcode.startsWith("$")) {
      // This only works when args are ALL at start
      asm.push({ ...cursor, pos: undefined });
      argCount++;
    } else asmPostProcessingCompleted = true; // as it exits as soon as a non $arg is found
  }
  const { asmDrops, hexDrops } = addOpDrops(argCount);
  console.log({ asmDrops, hexDrops });
  asmDrops.map((x) => {
    asm.push(x);
  });
  console.dir({ asm }, { depth: null });

  // Now we do the same to the hex
  let hexPostProcessingCompleted = false;
  let remainingHex = hex;
  while (!hexPostProcessingCompleted) {
    if (remainingHex[0] === "<") {
      // find the end of the arg
      let split = remainingHex.split(">");
      let arg = split[0] + ">";
      remainingHex = split[1];
      console.log({ arg });
      hex += arg;
      console.log({ hex });
    } else {
      // Add the equivalent argCount of OP_DROPs
      hex += hexDrops;
      hexPostProcessingCompleted = true;
    }
  }
  compiledContractResult.hex = hex;
  compiledContractResult.asm = asm;
  console.log({
    compiledContractResult,
    hex,
    asm,
    argCount,
  });
  return compiledContractResult;
};

function minIntToASM(intVal) {
  let intValData = new Int(intVal).toHex();
  const len = intValData.length / 2;
  // console.log({minIntToASM:{intValData,len,num2hex:num2hex(len,2)}})
  intValData = // intVal==-1?'OP_1NEGATE':
    intVal == 0
      ? "OP_0"
      : intVal == 1
      ? "OP_1"
      : intVal == 2
      ? "OP_2"
      : intVal == 3
      ? "OP_3"
      : intVal == 4
      ? "OP_4"
      : intVal == 5
      ? "OP_5"
      : intVal == 6
      ? "OP_6"
      : intVal == 7
      ? "OP_7"
      : intVal == 8
      ? "OP_8"
      : intVal == 9
      ? "OP_9"
      : intVal == 10
      ? "OP_10"
      : intVal == 11
      ? "OP_11"
      : intVal == 12
      ? "OP_12"
      : intVal == 13
      ? "OP_13"
      : intVal == 14
      ? "OP_14"
      : intVal == 15
      ? "OP_15"
      : intVal == 16
      ? "OP_16"
      : len <= 75
      ? num2hex(len, 2) + intValData
      : len <= 255
      ? "OP_PUSHDATA1 " + intValData
      : //len<=65535?
        "OP_PUSHDATA2 " + intValData;

  return intValData;
}
function minIntToHex(intVal) {
  const asm = minIntToASM(intVal);
  if (/^[0-9a-fA-F]+$/.test(asm)) {
    //|| asm === 0) {
    return bsv.Script(Buffer.from(asm, "hex")).toHex();
  } else {
    return bsv.Script.fromASM(minIntToASM(intVal)).toHex();
  }
}
function toBigIndian(hexstr) {
  return reverseEndian(hexstr);
}

function uint32Tobin(d) {
  var s = (+d).toString(16);
  if (s.length < 4) {
    s = "0" + s;
  }
  return toLittleIndian(s);
}

function num2hex(d, padding) {
  var s = Number(d).toString(16);
  // add padding if needed.
  while (s.length < padding) {
    s = "0" + s;
  }
  return s;
}
function num2hexByteCount(d, paddingBytes) {
  var s = Number(d).toString(16);
  // add padding if needed.
  while (s.length < paddingBytes * 2) {
    s = "0" + s;
  }
  return s;
}

async function createPayByOthersTx(address) {
  // step 1: fetch utxos
  let { data: utxos } = await axios.get(
    `${API_PREFIX}/address/${address}/unspent`
  );

  utxos = utxos.map((utxo) => ({
    txId: utxo.tx_hash,
    outputIndex: utxo.tx_pos,
    satoshis: utxo.value,
    script: bsv.Script.buildPublicKeyHashOut(address).toHex(),
  }));

  // step 2: build the tx
  const tx = new bsv.Transaction().from(utxos);

  return tx;
}

async function createLockingTxFromInput(fundingInput) {
  const tx = new bsv.Transaction().from([
    {
      txId: utxo.tx_hash,
      outputIndex: utxo.tx_pos,
      satoshis: utxo.value,
      script: bsv.Script.buildPublicKeyHashOut(address).toHex(),
    },
  ]);
  tx.addOutput(
    new bsv.Transaction.Output({
      script: new bsv.Script(), // place holder
      satoshis: amountInContract,
    })
  );
}

async function createLockingTx(address, amountInContract, fee) {
  // step 1: fetch utxos
  /*let {
    data: utxos
  } = await axios.get(`${API_PREFIX}/address/${address}/unspent`)
*/
  utxos = utxos.map((utxo) => ({
    txId: utxo.tx_hash,
    outputIndex: utxo.tx_pos,
    satoshis: utxo.value,
    script: bsv.Script.buildPublicKeyHashOut(address).toHex(),
  }));

  // step 2: build the tx
  const tx = new bsv.Transaction().from(utxos);
  tx.addOutput(
    new bsv.Transaction.Output({
      script: new bsv.Script(), // place holder
      satoshis: amountInContract,
    })
  );

  tx.change(address).fee(fee || minFee);

  return tx;
}

async function anyOnePayforTx(tx, address, fee) {
  // step 1: fetch utxos
  let { data: utxos } = await axios.get(
    `${API_PREFIX}/address/${address}/unspent`
  );

  utxos.map((utxo) => {
    tx.addInput(
      new bsv.Transaction.Input({
        prevTxId: utxo.tx_hash,
        outputIndex: utxo.tx_pos,
        script: new bsv.Script(), // placeholder
      }),
      bsv.Script.buildPublicKeyHashOut(address).toHex(),
      utxo.value
    );
  });

  tx.change(address).fee(fee);

  return tx;
}

function createUnlockingTx(
  prevTxId,
  inputAmount,
  inputLockingScriptASM,
  outputAmount,
  outputLockingScriptASM
) {
  const tx = new bsv.Transaction();

  tx.addInput(
    new bsv.Transaction.Input({
      prevTxId,
      outputIndex: inputIndex,
      script: new bsv.Script(), // placeholder
    }),
    bsv.Script.fromASM(inputLockingScriptASM),
    inputAmount
  );

  tx.addOutput(
    new bsv.Transaction.Output({
      script: bsv.Script.fromASM(
        outputLockingScriptASM || inputLockingScriptASM
      ),
      satoshis: outputAmount,
    })
  );

  tx.fee(inputAmount - outputAmount);
  console.log({ tx });
  return tx;
}

function decToHex(d, padding) {
  var hex = Number(d).toString(16);
  padding =
    typeof padding === "undefined" || padding === null
      ? (padding = 2)
      : padding;
  while (hex.length < padding || hex.length % 2 !== 0) {
    hex = "0" + hex;
  }
  return hex;
}

function unlockP2PKHInput(privateKey, tx, inputIndex, sigtype) {
  const sig = new bsv.Transaction.Signature({
    publicKey: privateKey.publicKey,
    prevTxId: tx.inputs[inputIndex].prevTxId,
    outputIndex: tx.inputs[inputIndex].outputIndex,
    inputIndex,
    signature: bsv.Transaction.Sighash.sign(
      tx,
      privateKey,
      sigtype,
      inputIndex,
      tx.inputs[inputIndex].output.script,
      tx.inputs[inputIndex].output.satoshisBN
    ),
    sigtype,
  });

  tx.inputs[inputIndex].setScript(
    bsv.Script.buildPublicKeyHashIn(
      sig.publicKey,
      sig.signature.toDER(),
      sig.sigtype
    )
  );
}

async function sendTx(tx) {
  const { data: txid } = await axios.post(`${API_PREFIX}/tx/raw`, {
    txhex: tx.toString(),
  });
  return txid;
}

async function compileContract(fileName, options) {
  const filePath = path.join(__dirname, "contracts", fileName);
  const out = path.join(__dirname, "deployments/fixture/autoGen");
  // console.log({ filePath, out })
  const result = await compileContractImpl(
    filePath,
    options
      ? options
      : {
          out: out,
          sourceMap: true,
        }
  );
  // console.log({result})
  if (result.errors.length > 0) {
    console.log(`Compile contract ${filePath} fail: `, result.errors);
    throw result.errors;
  }

  return result;
}

function compileTestContract(fileName) {
  const filePath = path.join(__dirname, "tests", "testFixture", fileName);
  const out = path.join(__dirname, "tests", "out");
  if (!existsSync(out)) {
    mkdirSync(out);
  }
  const result = compileContractImpl(filePath, {
    out: out,
  });
  if (result.errors.length > 0) {
    console.log(`Compile contract ${filePath} fail: `, result.errors);
    throw result.errors;
  }

  return result;
}

function loadDesc(fileName) {
  const filePath = path.join(
    __dirname,
    `deployments/fixture/autoGen/${fileName}`
  );
  if (!existsSync(filePath)) {
    throw new Error(
      `Description file ${filePath} not exist!\nIf You already run 'npm run watch', maybe fix the compile error first!`
    );
  }
  return JSON.parse(readFileSync(filePath).toString());
}

function showError(error) {
  // Error
  if (error.response) {
    // The request was made and the server responded with a status code
    // that falls out of the range of 2xx
    console.log(
      "Failed - StatusCodeError: " +
        error.response.status +
        ' - "' +
        error.response.data +
        '"'
    );
    // console.log(error.response.headers);
  } else if (error.request) {
    // The request was made but no response was received
    // `error.request` is an instance of XMLHttpRequest in the
    // browser and an instance of
    // http.ClientRequest in node.js
    console.log(error.request);
  } else {
    // Something happened in setting up the request that triggered an Error
    console.log("Error:", error.message);
    if (error.context) {
      console.log(error.context);
    }
  }
}

function padLeadingZero(hex) {
  if (hex.length % 2 === 0) return hex;
  return "0" + hex;
}

const dataPushItems = (hexStrings) => {
  var ASM = "";
  hexStrings.map((h) => {
    //ASM+=("OP_PUSHDATA1 "+(h.length/2).toString('16')+' '+h+' ')//OP_PUSHDATA1 = // max 255, next byte is oplen
    ASM += h + " ";
  });
  return ASM;
};

const dropItems = (count) => {
  var ASM = "";
  for (var i = count; i > 0; ) {
    if (i > 1) {
      ASM += "OP_2DROP ";
      i -= 2;
    } else {
      ASM += "OP_DROP ";
      i -= 1;
    }
  }
  return ASM;
};

// reverse hexStr byte order
function reverseEndian(hexStr, padding = 0) {
  // console.log({hexStr})
  let num = new BN(hexStr, "hex");
  let buf = num.toBuffer();
  let res = buf.toString("hex").match(/.{2}/g).reverse().join("");
  while (res.length < padding) res += "0";
  return res;
}

function reverseHexString(hexStr, paddingByteCount) {
  return Buffer.from(hexStr, "hex").reverse().toString("hex");
}

function hashBufFromTxid(hexStr) {
  return Buffer.from(hexStr, "hex").reverse();
}

function WoC(txid) {
  return `https://test.whatsonchain.com/tx/${txid}`;
}

const runSystemCmd = async (cmd) => {
  var exec = require("child_process").exec;
  function execute(command) {
    exec(command, function (error, stdout, stderr) {
      console.log(stdout);
    });
  }
  /*var ls  = spawn(cmd);
  ls.stdout.on('data', function (data) {
     console.log(data);
  });*/
  await execute(cmd);
  console.log(cmd);
};

const emptyPublicKey =
  "000000000000000000000000000000000000000000000000000000000000000000";

function calcTxid(rawTx) {
  return reverseEndian(bsv.crypto.Hash.sha256sha256(Buffer.from(rawTx, "hex")));
}

function decToHex(d, padding) {
  var hex = Number(d).toString(16);
  padding =
    typeof padding === "undefined" || padding === null
      ? (padding = 2)
      : padding;
  while (hex.length < padding || hex.length % 2 !== 0) {
    hex = "0" + hex;
  }
  return hex;
}

function removeASMDataDollars(asm) {
  const splitAsm = asm.split(" ");
  // console.log({ asm, splitAsm })
  let asmLogic = "";
  // let hasFoundFirst = false
  for (var i = 0; i < splitAsm.length; i++) {
    const opCode = splitAsm[i];
    // console.log({opCode, startsWith:opCode.startsWith('$')})
    if (!opCode.startsWith("$")) {
      //&& hasFoundFirst){
      asmLogic += opCode + (i === splitAsm.length - 1 ? "" : " ");
      //console.log(asmLogic)
    }
  }
  // console.log({asmLogic})
  return asmLogic;
}

function stripData(asm, headerLengthAsm = 0) {
  // console.log({asm})
  let splitAsm = asm.split(" ");
  splitAsm = splitAsm.slice(headerLengthAsm, splitAsm.length + 1);
  foundFirstData = false;
  let asmLogic = "";
  for (var i = 0; i < splitAsm.length; i++) {
    const opCode = splitAsm[i];
    if (!opCode.startsWith("$")) {
      //} && foundFirstData){
      asmLogic += opCode + (i === splitAsm.length - 1 ? "" : " ");
    }
    //else foundFirstData = true;
  }
  // console.log({ asm, strippedAsm: bsv.Script.fromASM(asmLogic).toHex() })
  return asmLogic;
}
// Xor taken from crypto-js
function xor(a, b) {
  if (!Buffer.isBuffer(a)) a = Buffer.from(a, "hex");
  if (!Buffer.isBuffer(b)) b = Buffer.from(b, "hex");
  var res = [];
  if (a.length > b.length) {
    for (var i = 0; i < b.length; i++) {
      res.push(a[i] ^ b[i]);
    }
  } else {
    for (var i = 0; i < a.length; i++) {
      res.push(a[i] ^ b[i]);
    }
  }
  return new Buffer.from(res).toString("hex");
}

const preimageParser = (hex) => {
  const preimage = Buffer.from(hex, "hex");
  const version = toHex(preimage.subarray(0, 4)); // 4 bytes
  const hashPrevouts = toHex(preimage.subarray(4, 36)); // 32 bytes
  const hashSequence = toHex(preimage.subarray(36, 68)); // 32 bytes
  const txid = toHex(preimage.subarray(68, 100)); //32 bytes txid
  const vout = toHex(preimage.subarray(100, 104)); // 4 bytes vout
  // const scriptLength = toHex(preimage.slice(104, 105))// bytes length of script
  const len = preimage.length;
  const lenAndScript = toHex(preimage.subarray(104, len - 52)); // the script
  const satoshis = toHex(preimage.subarray(len - 52, len - 44)); // amount 8 bytes
  const nSequence = toHex(preimage.subarray(len - 44, len - 40)); // 4 bytes
  const hashOutputs = toHex(preimage.subarray(len - 40, len - 8)); // 32 bytes
  const nLocktime = toHex(preimage.subarray(len - 8, len - 4)); // 4 bytes
  const nHashType = toHex(preimage.subarray(len - 4, len)); // 4 bytes
  return {
    version,
    hashPrevouts,
    hashSequence,
    txid,
    vout,
    lenAndScript,
    satoshis,
    nSequence,
    hashOutputs,
    nLocktime,
    nHashType,
  };
};

function genLaunchConfigFile(
  constructorArgs,
  pubFuncArgs,
  pubFunc,
  name,
  programFile,
  txContext,
  asmArgs
) {
  console.dir(
    {
      constructorArgs,
      pubFuncArgs,
      pubFunc,
      name,
      programFile,
      txContext,
      asmArgs,
    },
    { depth: null }
  );
  // some desc without sourceMap will not have file property.
  if (!programFile) {
    return "";
  }
  var debugConfig = {
    type: "scrypt",
    request: "launch",
    internalConsoleOptions: "openOnSessionStart",
    name: name,
    program: programFile,
    constructorArgs: constructorArgs,
    pubFunc: pubFunc,
    pubFuncArgs: pubFuncArgs,
  };
  var debugTxContext = {};
  if (!isEmpty(txContext)) {
    var tx = txContext.tx || "";
    var inputIndex = txContext.inputIndex || 0;
    var inputSatoshis = txContext.inputSatoshis || 0;
    if (tx) {
      Object.assign(debugTxContext, {
        hex: tx.toString(),
        inputIndex: inputIndex,
        inputSatoshis: inputSatoshis,
      });
    }
    if (typeof txContext.opReturn === "string") {
      Object.assign(debugTxContext, { opReturn: txContext.opReturn });
    } else if (typeof txContext.opReturnHex === "string") {
      Object.assign(debugTxContext, { opReturnHex: txContext.opReturnHex });
    }
  }
  if (!isEmpty(asmArgs)) {
    Object.assign(debugConfig, { asmArgs: asmArgs });
  }
  if (!isEmpty(debugTxContext)) {
    Object.assign(debugConfig, { txContext: debugTxContext });
  }
  var launch = {
    version: "0.2.0",
    configurations: [debugConfig],
  };
  var jsonstr = JSON.stringify(
    launch,
    function (key, value) {
      if (typeof value === "bigint") {
        return value.toString();
      } else {
        return value;
      }
    },
    2
  );
  if (isNode()) {
    // console.log("isNode")
    var filename = name + "-launch.json";
    // console.log({filename})
    var file = path.join(
      fs.mkdtempSync("" + os_1.tmpdir() + path.sep + "sCrypt."),
      filename
    );
    // console.log({file})
    fs.writeFileSync(file, jsonstr);
    return path2uri(file);
  } else {
    console.error(pubFunc + "() call fail, see launch.json", jsonstr);
  }
}

function opCodeSeparate(asm) {
  const split = asm.split("OP_CODESEPARATOR");
  const sigBit = split[split.length - 1].trim();
  // console.log({signatureBit:sigBit})
  return sigBit;
}

function getMultiSigHash(threshold, pubKeys) {
  let multiSigHash = new Buffer.from(
    Number(pubKeys.length).toString(16).padStart(2, "0"),
    "hex"
  );
  for (const pubK of pubKeys) {
    multiSigHash = Buffer.concat([multiSigHash, pubK.toBuffer()]);
  }
  multiSigHash = Buffer.concat([
    multiSigHash,
    Buffer.from(Number(threshold).toString(16).padStart(2, "0"), "hex"),
  ]);
  // A sha256 ripemd160 hash of that concatenated blob
  return Hash.sha256Ripemd160(multiSigHash);
}

//create an input spending from prevTx's output, with empty script
function createInputFromPrevTx(tx, outputIndex) {
  console.log("createInputFromPrevTx");
  const outputIdx = outputIndex || 0;
  return new bsv.Transaction.Input({
    prevTxId: tx.id,
    outputIndex: outputIdx,
    script: new bsv.Script(), // placeholder
    output: tx.outputs[outputIdx],
  });
}

const sleep = async (seconds) => {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve();
    }, seconds * 1000);
  });
};

async function deployContract(contract, amount) {
  const { privateKey } = require("./privateKey");
  const address = privateKey.toAddress();
  const tx = new bsv.Transaction();

  tx.from(await fetchUtxos(address))
    .addOutput(
      new bsv.Transaction.Output({
        script: contract.lockingScript,
        satoshis: amount,
      })
    )
    .change(address)
    .sign(privateKey);

  await sendTx(tx);
  return tx;
}

function createUnlockingTx(
  prevTxId,
  inputAmount,
  inputLockingScriptASM,
  outputAmount,
  outputLockingScriptASM
) {
  const tx = new bsv.Transaction();

  tx.addInput(
    new bsv.Transaction.Input({
      prevTxId,
      outputIndex: inputIndex,
      script: new bsv.Script(), // placeholder
    }),
    bsv.Script.fromASM(inputLockingScriptASM),
    inputAmount
  );

  tx.addOutput(
    new bsv.Transaction.Output({
      script: bsv.Script.fromASM(
        outputLockingScriptASM || inputLockingScriptASM
      ),
      satoshis: outputAmount,
    })
  );

  tx.fee(inputAmount - outputAmount);

  return tx;
}

//create an input spending from prevTx's output, with empty script
function createInputFromPrevTx(tx, outputIndex) {
  // console.log("createInputFromPrevTx")
  const outputIdx = outputIndex || 0;
  return new bsv.Transaction.Input({
    prevTxId: tx.id,
    outputIndex: outputIdx,
    script: new bsv.Script(), // placeholder
    output: tx.outputs[outputIdx],
  });
}

async function fetchUtxos(address) {
  // step 1: fetch utxos
  let { data: utxos } = await axios.get(
    `${API_PREFIX}/address/${address}/unspent`
  );

  return utxos.map((utxo) => ({
    txId: utxo.tx_hash,
    outputIndex: utxo.tx_pos,
    satoshis: utxo.value,
    script: bsv.Script.buildPublicKeyHashOut(address).toHex(),
  }));
}
// fixLowS increments the first input's sequence number until the sig hash is safe for low s.
function fixLowS(tx, lockingScript, inputSatoshis, inputIndex) {
  for (i = 0; i < 25; i++) {
    const preimage = getPreimage(tx, lockingScript, inputSatoshis, inputIndex);
    const sighash = bsv.crypto.Hash.sha256sha256(
      Buffer.from(toHex(preimage), "hex")
    );
    const msb = sighash.readUInt8();
    if (msb < MSB_THRESHOLD) {
      return;
    }
    tx.inputs[0].sequenceNumber++;
  }
}
//gitlab.com/elas-digital/amleh-fork

// checkLowS returns true if the sig hash is safe for low s.
function checkLowS(tx, lockingScript, inputSatoshis, inputIndex) {
  const preimage = getPreimage(tx, lockingScript, inputSatoshis, inputIndex);
  const sighash = bsv.crypto.Hash.sha256sha256(
    Buffer.from(toHex(preimage), "hex")
  );
  const msb = sighash.readUInt8();
  return msb < MSB_THRESHOLD;
}

function toBigIndian(hexstr) {
  return reverseEndian(hexstr);
}

function uint32Tobin(d) {
  var s = (+d).toString(16);
  if (s.length < 4) {
    s = "0" + s;
  }
  return toLittleIndian(s);
}
/**
 * inspired by : https://bigishdata.com/2017/11/13/how-to-build-a-blockchain-part-4-1-bitcoin-proof-of-work-difficulty-explained/
 * @param {*} bitsHex bits of block header, in big endian
 * @returns a target number
 */
function toTarget(bitsHex) {
  const shift = bitsHex.substr(0, 2);
  const exponent = parseInt(shift, 16);
  const value = bitsHex.substr(2, bitsHex.length);
  const coefficient = parseInt(value, 16);
  const target = coefficient * 2 ** (8 * (exponent - 3));
  return BigInt(target);
}

/**
 * convert pool difficulty to a target number
 * @param {*}  difficulty which can fetch by api https://api.whatsonchain.com/v1/bsv/<network>/chain/info
 * @returns target
 */
function pdiff2Target(difficulty) {
  if (typeof difficulty === "number") {
    difficulty = BigInt(Math.floor(difficulty));
  }

  return BigInt(toTarget("1d00ffff") / difficulty);
}

// serialize Header to get raw header
function serializeHeader(header) {
  return (
    uint32Tobin(header.version) +
    toLittleIndian(header.previousblockhash) +
    toLittleIndian(header.merkleroot) +
    uint32Tobin(header.time) +
    toLittleIndian(header.bits) +
    uint32Tobin(header.nonce)
  );
}

function decToHex(d, padding) {
  var hex = Number(d).toString(16);
  padding =
    typeof padding === "undefined" || padding === null
      ? (padding = 2)
      : padding;
  while (hex.length < padding || hex.length % 2 !== 0) {
    hex = "0" + hex;
  }
  return hex;
}

function isNode() {
  return typeof window === "undefined" && typeof process === "object";
}

function path2uri(path) {
  if (isNode()) {
    return url_1.pathToFileURL(path).toString();
  } else {
    return path;
  }
}

function genLaunchConfigFile(
  constructorArgs,
  pubFuncArgs,
  pubFunc,
  name,
  programFile,
  txContext,
  asmArgs
) {
  console.dir(
    {
      constructorArgs,
      pubFuncArgs,
      pubFunc,
      name,
      programFile,
      txContext,
      asmArgs,
    },
    { depth: null }
  );
  // some desc without sourceMap will not have file property.
  if (!programFile) {
    return "";
  }
  var debugConfig = {
    type: "scrypt",
    request: "launch",
    internalConsoleOptions: "openOnSessionStart",
    name: name,
    program: programFile,
    constructorArgs: constructorArgs,
    pubFunc: pubFunc,
    pubFuncArgs: pubFuncArgs,
  };
  var debugTxContext = {};
  if (!isEmpty(txContext)) {
    var tx = txContext.tx || "";
    var inputIndex = txContext.inputIndex || 0;
    var inputSatoshis = txContext.inputSatoshis || 0;
    if (tx) {
      Object.assign(debugTxContext, {
        hex: tx.toString(),
        inputIndex: inputIndex,
        inputSatoshis: inputSatoshis,
      });
    }
    if (typeof txContext.opReturn === "string") {
      Object.assign(debugTxContext, { opReturn: txContext.opReturn });
    } else if (typeof txContext.opReturnHex === "string") {
      Object.assign(debugTxContext, { opReturnHex: txContext.opReturnHex });
    }
  }
  if (!isEmpty(asmArgs)) {
    Object.assign(debugConfig, { asmArgs: asmArgs });
  }
  if (!isEmpty(debugTxContext)) {
    Object.assign(debugConfig, { txContext: debugTxContext });
  }
  var launch = {
    version: "0.2.0",
    configurations: [debugConfig],
  };
  var jsonstr = JSON.stringify(
    launch,
    function (key, value) {
      if (typeof value === "bigint") {
        return value.toString();
      } else {
        return value;
      }
    },
    2
  );
  if (isNode()) {
    // console.log("isNode")
    var filename = name + "-launch.json";
    // console.log({filename})
    var file = path.join(
      fs.mkdtempSync("" + os_1.tmpdir() + path.sep + "sCrypt."),
      filename
    );
    // console.log({file})
    fs.writeFileSync(file, jsonstr);
    return path2uri(file);
  } else {
    console.error(pubFunc + "() call fail, see launch.json", jsonstr);
  }
}

const buildConfigFile = (
  input,
  unlockAttempt,
  tx,
  idx,
  pubFuncName,
  asmVars,
  contractClass,
  tempFile = undefined,
  moveConstructorArgsToInlineAsm = false
) => {
  console.log("build");
  let constructorArgs = input.contract.scriptedConstructor.args.map((a) => {
    return a.value;
  });
  console.log({ constructorArgs });
  // Rifle through those args and add to asmVars if moveConstructorArgsToInlineAsm
  if (moveConstructorArgsToInlineAsm) {
    console.log("In here");
    /*
            asmVars["PShaCheckpoint.unlock.genesisTxid"] = "0000000000000000000000000000000000000000000000000000000000000000",
            asmVars["PShaCheckpoint.unlock.pubKeyHash"] = "d8f3afa3e53255d2fb53ff71bb01493685d29f54",
            asmVars["PShaCheckpoint.unlock.parentTxid"] = "0000000000000000000000000000000000000000000000000000000000000000",
            asmVars["PShaCheckpoint.unlock.parentScriptHash"] = "0000000000000000000000000000000000000000000000000000000000000000"
*/
    input.contract.scriptedConstructor.args.map((a) => {
      console.log({ a });
      asmVars[`${contractClass.contractName}.unlock.${a.name}`] =
        a.value._value;
    });
    console.log({ asmVars });
    constructorArgs = [];
  }
  const pubFuncValues = unlockAttempt.args.map((a) => {
    return a.value;
  });
  const txContext = {
    tx,
    inputIndex: idx,
    inputSatoshis: input.txOut.valueBn.toNumber(),
  };
  console.log({ pubFuncValues, txContext, contractClass });
  return genLaunchConfigFile(
    constructorArgs,
    pubFuncValues,
    pubFuncName,
    contractClass.abiCoder.contractName,
    contractClass.artifact.file,
    txContext,
    asmVars
  );
};

const genRabinSigPadding = (paddingByteCount) => {
  let paddingBytes = "";
  for (let i = 0; i < paddingByteCount; i++) {
    paddingBytes += "00";
  }
  return paddingBytes;
};

function bnToHex(bn) {
  var base = 16;
  var hex = BigInt(bn).toString(base);
  if (hex.length % 2) {
    hex = "0" + hex;
  }
  return hex;
}
function bnToHexLE(bn) {
  var base = 16;
  var hex = toLittleIndian(BigInt(bn).toString(base));

  if (hex.length % 2) {
    hex = "0" + hex;
  }
  return hex + "00";
}

module.exports = {
  inputIndex,
  inputSatoshis,
  newTx,
  createPayByOthersTx,
  createLockingTx,
  createUnlockingTx,
  DataLen,
  dummyTxId,
  reversedDummyTxId,
  reverseEndian,
  unlockP2PKHInput,
  sendTx,
  compileContract,
  loadDesc,
  sighashType2Hex,
  showError,
  compileTestContract,
  padLeadingZero,
  anyOnePayforTx,
  emptyPublicKey,
  dataPushItems,
  dropItems,
  decToHex,
  WoC,
  runSystemCmd,
  removeASMDataDollars,
  xor,
  preimageParser,
  opCodeSeparate,
  calcTxid,
  getMultiSigHash,
  num2hex,
  toLittleIndian,
  stripData,
  createInputFromPrevTx,
  sleep,
  fixLowS,
  checkLowS,
  deployContract,
  fetchUtxos,
  toBigIndian,
  uint32Tobin,
  toTarget,
  pdiff2Target,
  serializeHeader,
  calcTxid,
  decToHex,
  getMultiSigHash,
  createUnlockingTx,
  minIntToASM,
  minIntToHex,
  num2hexByteCount,
  hashBufFromTxid,
  reverseHexString,
  cloneArgsToEndOfScript,
  genLaunchConfigFile,
  buildConfigFile,
  genRabinSigPadding,
  bnToHex,
  bnToHexLE,
};
