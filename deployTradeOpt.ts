import { TOKENOVATE_KSAA_SPOT_ASM } from './src/contracts/tradeOpt'
import {
    bsv,
    TestWallet,
    DefaultProvider,
    PubKey,
    PubKeyHash,
    findSig,
    toHex,
    MethodCallOptions,
    toByteString,
} from 'scrypt-ts'

import * as dotenv from 'dotenv'

// Load the .env file
dotenv.config()

// Read the private key from the .env file.
// The default private key inside the .env file is meant to be used for the Bitcoin testnet.
// See https://scrypt.io/docs/bitcoin-basics/bsv/#private-keys
const privateKey = bsv.PrivateKey.fromWIF(process.env.PRIVATE_KEY || '')

// Prepare signer.
// See https://scrypt.io/docs/how-to-deploy-and-call-a-contract/#prepare-a-signer-and-provider
const signer = new TestWallet(
    privateKey,
    new DefaultProvider({
        network: bsv.Networks.testnet,
    })
)

const testTradeJson = {
    "sessionId": "d88341c7-d49f-4336-b991-c1119f3b1211",
    "buyer": {
      "publicKey": "020202020202020202020202020202020202020202020202020202020202020202",
      "custodian": "Elas bank"
    },
    "seller": {
      "publicKey": "020303030303030303030303030303030303030303030303030303030303030303",
      "custodian": "Elas carbon"
    },
    "executionId": "23458520",
    "symbol": "KSAA-SPOT",
    "quantity": "13.00000",
    "price": "1733.321",
    "transactionTime": "2023-04-01T23:30:04.068Z",
    "msgOrigin": "GMEX",
    "msgUID": "dj3n09-8ebc-48fc-9d8c-4da7b9beacb9",
    "msgType": "TRADE",
    "msgTimestamp": 1683066604775,
    "capturedAt": 1683066605822,
    "eventId": "908ee2c5-a041-4fec-b466-5f206a696bcc"
  }
console.log(JSON.stringify(testTradeJson))
async function main() {
    await TOKENOVATE_KSAA_SPOT_ASM.compile()

    // TODO: Adjust the amount of satoshis locked in the smart contract:
    const amount = 1
    const adminWIF = 'cTWpMoCA7mrhG4L11j3RYdTUq2fkLxC4t6ZkZQKGrNt9yB3vUrE4'
    const adminKey = bsv.PrivateKey.fromWIF(adminWIF)
    const adminPubKeyHash = bsv.crypto.Hash.sha256ripemd160(
        adminKey.publicKey.toBuffer()
    ).toString('hex')

    // public key of the `privateKey`
    const publicKey = privateKey.publicKey
    const instance = new TOKENOVATE_KSAA_SPOT_ASM(
        // TODO: Adjust constructor parameter values:
        PubKey(publicKey.toHex()),
        PubKeyHash(adminPubKeyHash)
    )

    // Connect to a signer.
    await instance.connect(signer)

    // Contract deployment.
    const deployTx = await instance.deploy(amount)
    console.log(
        'Contract deployed: https://test.whatsonchain.com/tx/' + deployTx?.id
    )
    // bind a customized tx builder for the public method `MyContract.unlock`
    instance.bindTxBuilder("unlock", (current: TOKENOVATE_KSAA_SPOT_ASM, options: MethodCallOptions<TOKENOVATE_KSAA_SPOT_ASM>, ...args: any) => {
        // the tx is NOT signed
        const unsignedTx: bsv.Transaction = new bsv.Transaction()
            // add contract input
            .addInput(current.buildContractInput(options.fromUTXO))
            // add a p2pkh output
            .addOutput(new bsv.Transaction.Output({
                script: bsv.Script.fromHex(
                    '006a4d' +
                    // Tokenovate have created a new TRADE
                    '546f6b656e6f76617465206861766520637265617465642061206e657720' + Buffer.from(testTradeJson.msgType,'utf-8').toString('hex') +
                    // event between: buyer: buyerPubKey
                    '206576656e74206265747765656e3a0a42757965723a20' + Buffer.from(testTradeJson.buyer.publicKey,'utf-8').toString('hex') +
                    // buyer custodian: buyerCustodian
                    '0a427579657220637573746f6469616e3a20' + Buffer.from(testTradeJson.buyer.custodian,'utf-8').toString('hex') +
                    // and Seller: sellerPubKey
                    '0a616e640a53656c6c65723a20' + Buffer.from(testTradeJson.seller.publicKey,'utf-8').toString('hex') +
                    // Seller custodian: sellerCustodian
                    '0a53656c6c657220637573746f6469616e3a20' + Buffer.from(testTradeJson.seller.custodian,'utf-8').toString('hex') +
                    // For qty
                    '0a0a666f7220' + Buffer.from(testTradeJson.quantity,'utf-8').toString('hex') +
                    // units KSAA-SPOT
                    '20756e69747320' + Buffer.from(testTradeJson.symbol,'utf-8').toString('hex') +
                    // At a price of price
                    '2061742061207072696365206f662024' + Buffer.from(testTradeJson.price,'utf-8').toString('hex') +
                    // USD each. The trade venue is GMEX
                    '2055534420656163682e0a0a5468652074726164652076656e756520697320' + Buffer.from(testTradeJson.msgOrigin,'utf-8').toString('hex') +
                    // The trade was completed at tradeTimestamp
                    '0a0a546869732074726164652077617320636f6d706c6574656420617420' + Buffer.from(testTradeJson.transactionTime,'utf-8').toString('hex') +
                    // For more information see https://receiptviewer.tokenovate.com/trade/executionID
                    '0a0a466f72206d6f726520696e666f726d6174696f6e207365653a2068747470733a2f2f726563656970747669657765722e746f6b656e6f766174652e636f6d2f74726164652f' + Buffer.from(testTradeJson.executionId,'utf-8').toString('hex') +
                    // ledger
                    '0a4b5341412d53706f742074726164652056657273696f6e3a20302e310a54686973207472616465206973207265636f72646564206f6e2074686520546f6b656e6f76617465207472616465206c65646765722e',),
                satoshis: amount
            }))
            // add change output
            // .change(publicKey.toAddress())
            console.dir({unsignedTxOut: unsignedTx?.outputs[0].script.toBuffer().toString('hex')})

        return Promise.resolve({
            tx: unsignedTx,
            atInputIndex: 0, // the contract input's index
            nexts: []
        })
    })
    // call
    const { tx: callTx } = await instance.methods.unlock(
        toByteString(
            '7b0a20202273657373696f6e4964223a202264383833343163372d643439662d343333362d623939312d633131313966336231323131222c0a2020226275796572223a207b0a20202020227075626c69634b6579223a2022303230323032303230323032303230323032303230323032303230323032303230323032303230323032303230323032303230323032303230323032303230323032222c0a2020202022637573746f6469616e223a2022456c61732062616e6b220a20207d2c0a20202273656c6c6572223a207b0a20202020227075626c69634b6579223a2022303230333033303330333033303330333033303330333033303330333033303330333033303330333033303330333033303330333033303330333033303330333033222c0a2020202022637573746f6469616e223a2022456c617320636172626f6e220a20207d2c0a202022657865637574696f6e4964223a20223233343538353230222c0a20202273796d626f6c223a20224b5341412d53504f54222c0a2020227175616e74697479223a202231332e3030303030222c0a2020227072696365223a2022313733332e333231222c0a2020227472616e73616374696f6e54696d65223a2022323032332d30342d30315432333a33303a30342e3036385a222c0a2020226d73674f726967696e223a2022474d4558222c0a2020226d7367554944223a2022646a336e30392d386562632d343866632d396438632d346461376239626561636239222c0a2020226d736754797065223a20225452414445222c0a2020226d736754696d657374616d70223a20313638333036363630343737352c0a20202263617074757265644174223a20313638333036363630353832322c0a2020226576656e744964223a202239303865653263352d613034312d346665632d623436362d356632303661363936626363220a7d'
        ),
        (sigResps) => findSig(sigResps, publicKey),
        PubKey(toHex(publicKey)),
        toByteString(
            '1224224215092742150b1908100912080f081918130410221105150d120d10240300000000'
        ),
        {
            pubKeyOrAddrToSign: publicKey,
        } as MethodCallOptions<TOKENOVATE_KSAA_SPOT_ASM>
    )

    console.log(
        'Contract called: https://test.whatsonchain.com/tx/' + callTx?.id
    )
}

main()
